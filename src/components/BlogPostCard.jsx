import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import he from 'he';
import '../assets/styles/BlogPostCard.css';

export default function BlogPostCard({post}) {
  // Convert byte data to a data URL
  const imageDataUrl = `data:image/jpeg;base64,${post.imageData}`;

  const decodedContent = he.decode(post.content);

  // Parse the JSON-like content
  const parsedContent = JSON.parse(decodedContent);

  // Extract the text
  const plainText = parsedContent.blocks.map((block) => block.text).join('\n');

  return (
    <div className='card-container'>
      <div className='card'>
        <figure className='card-thumb'>
          {post.imageData && (
            <img
              className='card-image'
              src={imageDataUrl}
              alt='Post Image'
            />
          )}
          <figcaption className='card-caption'>
            <h2 className='card-title'>{post.title}</h2>
            <p className='card-snippet'>{plainText.slice(0, 100)}</p>
            <Link
              to={`/posts/${post.id}`}
              className='card-button'>
              Read more
            </Link>
          </figcaption>
        </figure>
      </div>
    </div>
  );
}

BlogPostCard.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    imageData: PropTypes.string.isRequired,
  }).isRequired,
};
