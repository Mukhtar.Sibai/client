import PropTypes from 'prop-types';
import {Editor, EditorState, convertFromRaw} from 'draft-js';
import he from 'he';
import '../assets/styles/post.css';
import UserService from '../services/UserService';
import httpClient from '../httpClient';
import {useNavigate} from 'react-router-dom';
export default function Post({post}) {
  const navigate = useNavigate();
  // Convert byte data to a data URL
  const imageDataUrl = `data:image/jpeg;base64,${post.imageData}`;

  // Decode HTML entities before parsing JSON
  const decodedContent = he.decode(post.content);

  // Convert the decoded content to EditorState
  const contentState = convertFromRaw(JSON.parse(decodedContent));
  const editorState = EditorState.createWithContent(contentState);
  let postId = post.id;

  const deletePost = async () => {
    try {
      const response = await httpClient.delete(`/posts/${postId}/delete`, {
        headers: {
          Authorization: `Bearer ${UserService.getToken()}`,
        },
      });

      if (response.status !== 200) {
        console.error('Error deleting post:', response.statusText);
      } else {
        console.log('Post deleted successfully');
        navigate('/posts');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div className='post-container'>
      <div className='image-container'>
        {post.imageData && (
          <img
            src={imageDataUrl}
            alt='Post Image'
          />
        )}
      </div>
      <div className='content-container'>
        <div className='post-header'>
          <h2>{post.title}</h2>
          <h3>Written by {post.authorID}</h3>
          <div className='timestamp-container'>
            <p>
              Created At:
              <time dateTime={post.createdAt}>
                {new Date(post.createdAt).toLocaleString()}
              </time>
            </p>
            {post.modifiedAt && (
              <p>
                Last updated:
                <time dateTime={post.modifiedAt}>
                  {new Date(post.modifiedAt).toLocaleString()}
                </time>
              </p>
            )}
            {UserService.hasRole(['ADMIN']) && (
              <button onClick={deletePost}>Delete Post</button>
            )}
          </div>
        </div>
        {/* Use Editor component to render formatted content */}
        <Editor
          editorState={editorState}
          readOnly
        />
      </div>
    </div>
  );
}

Post.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    imageData: PropTypes.string.isRequired,
    authorID: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    modifiedAt: PropTypes.string,
  }).isRequired,
};
