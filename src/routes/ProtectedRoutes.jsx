import UserService from '../services/UserService';
import {Outlet} from 'react-router-dom';

export default function ProtectedRoutes() {
  return UserService.isLoggedIn() ? <Outlet /> : UserService.doLogin();
}
