import UserService from '../services/UserService';

export default function Home() {
  return (
    <div>
      <h1>Home</h1>
      {UserService.isLoggedIn() ? (
        <button onClick={() => UserService.doLogout()}>Logout</button>
      ) : (
        <button onClick={() => UserService.doLogin()}>Login</button>
      )}
    </div>
  );
}
