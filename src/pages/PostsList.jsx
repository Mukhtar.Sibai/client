import {useEffect, useState} from 'react';
import httpClient from '../httpClient';
import BlogPostCard from '../components/BlogPostCard';
import '../assets/styles/postsList.css';

export default function PostsList() {
  const [posts, setPosts] = useState([]);
  const [filteredPosts, setFilteredPosts] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');

  // Fetch the list of posts
  const fetchPosts = async () => {
    try {
      const response = await httpClient.get('/posts');
      setPosts(response.data);
      setFilteredPosts(response.data);
    } catch (error) {
      console.error('Error fetching posts:', error);
    }
  };

  useEffect(() => {
    fetchPosts();
  }, []);

  const handleSearch = () => {
    const filtered = posts.filter((post) =>
      post.title.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setFilteredPosts(filtered);
  };

  useEffect(() => {
    console.log('Posts:', posts);
    console.log('Filtered Posts:', filteredPosts);
  }, [posts, filteredPosts]);

  return (
    <div>
      <h2>Posts</h2>

      {/* Search Input */}
      <input
        type='text'
        placeholder='Search by name'
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <button onClick={handleSearch}>Search</button>

      {/* Display Filtered Posts */}

      <div className='posts-list'>
        {filteredPosts.map((post) => (
          <BlogPostCard
            key={post.id}
            post={post}
          />
        ))}
      </div>
    </div>
  );
}
