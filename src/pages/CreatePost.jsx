import {useState, useEffect} from 'react';
import {EditorState, convertToRaw} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import DOMPurify from 'dompurify';
import UserService from '../services/UserService';
import {useNavigate} from 'react-router-dom';
import httpClient from '../httpClient';
import '../assets/styles/createPost.css';

export default function CreatePost() {
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );
  const [title, setTitle] = useState('');
  const [image, setImage] = useState(null);
  const [createdPostID, setCreatedPost] = useState(null);
  const [formErrors, setFormErrors] = useState({});
  const navigate = useNavigate();

  const handleTitleChange = (e) => {
    const inputValue = e.target.value;
    // Ensure the title contains only alphanumeric characters and basic punctuation
    const sanitizedTitle = inputValue.replace(/[^\w\s.,!?]/g, '');
    setTitle(sanitizedTitle);
  };

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    setImage(file);
  };

  const validateForm = () => {
    const errors = {};

    // Validate title
    if (!title.trim()) {
      errors.title = 'Title is required';
    }

    // Validate image
    if (!image) {
      errors.image = 'Image is required';
    }

    // Validate editor content
    const contentState = editorState.getCurrentContent();
    const contentText = contentState.getPlainText().trim();

    if (!contentText) {
      errors.editor = 'Content is required';
    }

    setFormErrors(errors);
    return Object.keys(errors).length === 0;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!validateForm()) {
      return;
    }

    // Convert the editorState to raw content state
    const rawContentState = convertToRaw(editorState.getCurrentContent());

    // Serialize the raw content state to a JSON string
    const contentJsonString = JSON.stringify(rawContentState);

    // Sanitize the JSON string using DOMPurify
    const sanitizedContentJsonString = DOMPurify.sanitize(contentJsonString);

    const formData = new FormData();
    formData.append(
      'postCreationDTO',
      new Blob(
        [
          JSON.stringify({
            title: title,
            content: sanitizedContentJsonString,
          }),
        ],
        {
          type: 'application/json',
        }
      )
    );
    formData.append('imageFile', image);

    try {
      const response = await httpClient.post('/posts/create', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${UserService.getToken()}`,
        },
      });

      if (response.status === 201) {
        console.log('Post created successfully!');
        const match = response.data.match(
          /Post ID: (\w{8}-\w{4}-\w{4}-\w{4}-\w{12})/
        );
        const postId = match ? match[1] : null;
        console.log('Post ID:', postId);
        setCreatedPost(postId);
      } else {
        console.error('Failed to create post');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  useEffect(() => {
    if (createdPostID) {
      // Redirect to the post details page with the post ID
      navigate(`/posts/${createdPostID}`);
    }
  }, [createdPostID, navigate]);

  return (
    <div className='form-container'>
      <h2>Tell us your story</h2>
      <form onSubmit={handleSubmit}>
        {/* For Title */}
        <div className='form-group'>
          <label htmlFor='title'>Title:</label>
          <input
            type='text'
            id='title'
            className={`form-control ${formErrors.title ? 'error' : ''}`}
            name='title'
            placeholder='Title'
            value={title}
            onChange={handleTitleChange}
          />
          {formErrors.title && (
            <p className='error-message'>{formErrors.title}</p>
          )}
        </div>

        {/* For Image */}
        <div className='form-group'>
          <label htmlFor='image'>Image:</label>
          <div className={`input-group ${formErrors.image ? 'error' : ''}`}>
            <span className='input-group-btn'>
              <label className='btn btn-primary btn-file'>
                Browse
                <input
                  type='file'
                  id='image'
                  accept='image/*'
                  onChange={handleImageChange}
                />
              </label>
            </span>
            <input
              type='text'
              className='form-control'
              readOnly
            />
          </div>
          {formErrors.image && (
            <p className='error-message'>{formErrors.image}</p>
          )}
        </div>

        {/* For Editor */}
        <div className={`form-group ${formErrors.editor ? 'error' : ''}`}>
          <Editor
            editorState={editorState}
            toolbarClassName='toolbarClassName'
            wrapperClassName='wrapperClassName'
            editorClassName='editorClassName'
            onEditorStateChange={setEditorState}
          />
          {formErrors.editor && (
            <p className='error-message'>{formErrors.editor}</p>
          )}
        </div>
        <button type='submit'>Publish</button>
      </form>
    </div>
  );
}
