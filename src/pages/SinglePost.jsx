import {useParams} from 'react-router-dom';
import httpClient from '../httpClient';
import {useState, useEffect} from 'react';
import Post from '../components/Post';

export default function SinglePost() {
  const {postId} = useParams();
  const [singlePost, setSinglePost] = useState(null);

  // Fetch the single post data based on the postId
  const fetchSinglePost = async () => {
    try {
      const response = await httpClient.get(`/posts/${postId}`);
      return response.data;
    } catch (error) {
      console.error('Error fetching single post:', error);
      throw error;
    }
  };

  useEffect(() => {
    const getSinglePost = async () => {
      try {
        const post = await fetchSinglePost();
        setSinglePost(post);
      } catch (error) {
        console.log(error);
      }
    };

    getSinglePost();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [postId]);

  if (!singlePost) {
    return <p>Loading...</p>;
  }

  return <Post post={singlePost} />;
}
