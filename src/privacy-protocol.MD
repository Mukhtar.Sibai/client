# Wander Tales Privacy Protocol

**Latest Revision:** 2024-01-25

## Info Capture:

1. **Personal Data:**

   - Details like names and email addresses submitted by users.

2. **Browser Intel:**
   - Information scooped from users' web browsers during blog visits.

## Usage Guidelines:

1. **Tailoring:**

   - Customizing blog content based on user preferences.

2. **Information Alerts:**
   - Notifying users about fresh, pertinent updates.

## Info Confidentiality:

1. Wander Tales abstains from divulging or peddling personal details to external parties.

## Browser Biscuits:

1. Wander Tales employs cookies to enhance the user experience.

## Safeguard Measures:

1. Robust measures are in place to shield personal data from unwarranted access, tampering, or deletion.

## User Entitlements:

1. **Access Entitlements:**

   - Users retain the right to access their personal data.

2. **Correction Leverage:**

   - Users can request rectification of any inaccuracies in their personal information.

3. **Deletion Authority:**
   - Users can opt for the complete deletion of their Wander Tales account, eradicating all personal data.

## Policy Adjustments:

1. Wander Tales retains the prerogative to modify and refresh its privacy protocol.
2. Alterations will be communicated on the page, with the latest revision date prominently displayed.

## Contact Channels:

- Users can reach out to Wander Tales through the "info@wander-tales.se" for any queries regarding the policy.

## User Concord:

- By utilizing Wander Tales, users implicitly agree to abide by the stipulations of this privacy protocol.
