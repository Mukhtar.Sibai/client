import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import UserService from './services/UserService.js';
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from 'react-router-dom';
import Root from './routes/Root.jsx';
import Error from './routes/Error.jsx';
import Home from './pages/Home.jsx';
import PostsList from './pages/PostsList.jsx';
import SinglePost from './pages/SinglePost.jsx';
import ProtectedRoutes from './routes/ProtectedRoutes.jsx';
import CreatePost from './pages/CreatePost.jsx';

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route
      path='/'
      element={<Root />}
      errorElement={<Error />}>
      <Route
        index
        element={<Home />}
      />
      <Route
        path='posts'
        element={<PostsList />}
      />
      <Route
        path='/posts/:postId'
        element={<SinglePost />}
      />
      <Route element={<ProtectedRoutes />}>
        <Route
          element={<CreatePost />}
          path='/create'
          exact
        />
      </Route>
    </Route>
  )
);

const renderApp = () =>
  ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
      <RouterProvider router={router} />
    </React.StrictMode>
  );

UserService.initKeycloak(renderApp);
